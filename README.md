# Slimmer handhaven

Delen, verbeteren en het verder uitwerken van ideeën/code opgedaan in de sessie Slimmer Handhaven, 13-16 oktober 2019.


## Het idee van deze Gitlab
Het is aan iedereen vrij om hun eventuele, code, idee, whatever, te delen. Daarnaast kan dit een mooi platform zijn om samen verder te werken aan de uitwerking van de ideeën van de twee winnaars. Handhavers kunnen via het [Issueboard](https://gitlab.com/wsnl/slimmerhandhaven/-/boards), terwijl de codegekkies lekker los kunnen gaan op openstaande code issues.
Graag tot een volgende keer en laten we er samen een succes van maken (want als wij het niet doen wie dan wel?). 

## Hulp nodig?
Mocht je vragen hebben over Gitlab of wat dan ook kan je mij benaderen, t.deurloo@brabantsedelta.nl of +31 76 564 10 41

## En verder....
Ik hoorde hier en daar al leuke initiatieven, laten we in ieders geval kijken of we vaker samen dingen kunnen ondernemen (en toch niet al stiekem aan het idee van team Rigoreus gaan bouwen ;-) ). Mocht je nog ideeën hebben, casussen, wat dan ook ik zal een issue openen (zie dus het [Issueboard](https://gitlab.com/wsnl/slimmerhandhaven/-/boards)). .
